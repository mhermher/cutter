import Graph from './graph';

let g : Graph = new Graph();

g.addNode('hello');
g.addNode('world');
g.addEdge(['hello', 'world']);

g.seal();