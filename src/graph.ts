class Vertex {
    public edges : {[key : string] : EdgeInfo};/*
        all edges connected to the current vertex*/
    private visited : boolean = false;/*
        marker for whether the current vertex has been visited during the
        initial DFS, this is needed to identify back edges*/
    private upindex : number;/*
        marker for whether the current vertex has been visited during walkup.
        Set to number so that it does not have to be cleared and is instead
        iterated to the next walkup initiating vertex index*/
    private comp : Component;/*
        the parent component of the vertex*/
    private depth : number;/*
        DFS depth, 0-based*/
    private index : number;/*
        unique (within component) index of vertex. Passed into walkup
        initialization*/
    private lowpoint : number;/*
        lowpoint of the current vertex. The lowest DFS depth that can be reached
        through 0 or more DFS edges plus one back edge*/
    private leastanc : number;/*
        Similar to the idea of a low point, but it is the least DFS depth
        reachable by just a single back edge*/
    private parent : Vertex;/*
        DFS parent of the current vertex*/
    private deiseil : Vertex;/*
        the clock-wise next Vertex. Initialized to DFS parent*/
    private tuathail : Vertex;/*
        the counter-clock-wise next Vertex. Initialized to DFS parent*/
    private children : DFChildren;/*
        separated DFS child list. Represents all DFS children sorted by their
        lowpoints, ascending*/
    private roots : Vertex[];/*
        DFS child vertices which each represent a child biconnected component
        which are pertinent for the current walkup-walkdown iteration. Interally
        active ones precede externally active ones.*/
    private backflag : boolean = false;/*
        represents the target of a backedge for the current walkup-walkdown
        iteration.*/
    constructor(public graph : Graph, public name : string){
        this.edges = {};
    }
    public reset() : Vertex {
        this.comp = undefined;
        this.depth = undefined;
        this.lowpoint = undefined;
        this.visited = false;
        return(this);
    }/*
        When parent Graph needs to be altered w.r.t V/E sets, unset the
        properties determined for the Vertex by DFS/walking procedures*/
    public addEdge(edge : Edge, key : string, right : boolean) : Vertex {
        this.edges[key] = {
            edge : edge,
            right : right,
            back : undefined,
            lowpoint : undefined
        };
        return(this);
    }
    public visit(info : EnterInfo) : ExitInfo {
        let retInfo : ExitInfo;/*
            the object to be returned*/
        if (this.visited){
            this.edges[info.last].back = true;/*
                set the edge that led into the iteration as a back edge, because
                this vertex has already been visited. Can only be re-visited in
                case of a back edge.*/
            retInfo = {
                index : info.index,
                lowpoint : this.depth,
                depth : this.depth
            }/*
                pass back this Vertex's depth as a lowpoint. Do not iterate the
                index since no new vertex has been visited here.*/
            return(retInfo);
        } else {
            this.visited = true;/*
                set visit marker to true, any subsetquent visits to this vertex
                will trigger detection of a backedge.*/
            this.comp = info.comp.addNode(this);/*
                add the visited vertex to the parent component of the visitor.
                When initialized the component only gets a single vertex that is
                becomes the root of the DFS, all subsequent vertices visited
                from that root and beyond need to be added into the component*/
            this.depth = info.depth;/*
                the visitor tells us what depth we should the current vertex
                to*/
            this.index = info.index;/*
                the visitor tells us what index to set the vertex to.*/
            this.lowpoint = info.depth;/*
                initialize lowpoint to depth. It eventually becomes the min of
                the lowpoints of its DFS children.*/
            this.leastanc = info.depth;/*
                initialize the least ancestor to own depth*/
            this.parent = this.graph.getNode(info.last);/*
                the DFS parent of the current vertex is the vertex that called
                the current visit*/
            this.deiseil = this.parent;/*
                each non-backedge in a DFS is initially represented as a
                biconnected component and each vertex is a cut vertex between
                two of them. The outer face of each biconnected component is
                going to be represented by a circular doubly-linked list of
                vertices linked clockwise (deiseil) and counterclockwise
                (tuathail). Since each vertex is in two binconneted components
                to start. Init these members only with respect to the upward
                biconnected component, where the parent is the next node moving
                in either direction.*/
            this.tuathail = this.parent;/*
                See above*/
            this.graph.didVisit(this.name);/*
                The parent graph also needs to know which vertices have been
                visited, so that it knows when to start a new component. Remove
                it from the list of possible roots in the parent when it gets
                visited here*/
            let nextInfo : EnterInfo;/*
                initialize the object that is going to be passed into the
                subsequent visits we are going to call here.*/
            let getInfo : ExitInfo;/*
                initialize the object that will be returned from the subsequent
                visits we are going to call here.*/
            let nextIndex : number = info.index + 1;/*
                Iterate the index to be passed into the next visit call so that
                each one is unique*/
            for (let key in this.edges){
                let iterEdge : EdgeInfo = this.edges[key];/*
                    get the edgeinfo object of the next child to be visited*/
                if (iterEdge.back){
                    continue;/*
                        do not visit back vertices that were marked as backedges
                        by other earlier visits.*/
                } else {
                    nextInfo = {
                        comp : this.comp,
                        index : nextIndex,
                        depth : info.depth + 1,
                        last : this.name
                    }/*
                        pass into this visitor the current component, an
                        iterated index, the next depth level, and the current
                        vertex's name*/
                    getInfo = iterEdge.edge.visit(nextInfo, iterEdge.right);/*
                        visit the next edge and get information back.*/
                    iterEdge.lowpoint = getInfo.lowpoint;/*
                        Save the lowpoint of each edge*/
                    this.lowpoint = Math.min(this.lowpoint, getInfo.lowpoint);/*
                        set the low point of the current vertex to a running
                        minimum*/
                    this.leastanc = Math.min(this.leastanc, getInfo.depth);/*
                        set the leastanc of the current vertex to a running
                        minimum. This will only be lower if the vertex being
                        visited here is a back edge*/
                    nextIndex = getInfo.index;/*
                        get the next index (which has iterated through all of
                        the newly added vertices.*/
                    this.children.addChild(iterEdge);/*
                        add to the children object which represents the
                        separated DFS child list. This object will have its
                        members removed one by one as biconnected components
                        are merged when embedding a new back edge.*/
                }
            }
            this.children.seal();/*
                This calls the sorter of the children object so that they are
                sorted by lowpoint ascending. This is important so that we can
                always get the minimum lowpoint of children just by checking the
                first member. The object is a special class that sorts linearly
                and keeps deletions to constant time*/
            retInfo = {
                index : nextIndex,
                lowpoint : this.lowpoint,
                depth : this.depth
            };/*
                pass back to the visiting vertex the next index and this
                vertex's lowpoint*/
            return(retInfo);
        }
    }/*
        DFS iterator, written recursively. perform DFS unit actions, then call
        same method for all non-back-edge edges. The edge DFS method just passes
        the call down to the other vertex of the edge. After calling self on
        edges. Perform walkup and walkdown. Since walkup and walkdown only
        affect the t-sorted farther part of the graph, they can be called from
        with the DFS iterator.*/
    public walkup(initindex : number) : Vertex {
        this.upindex = initindex;
        this.backflag = true;
        let dodeis : boolean = true;
        let dotuat : boolean = true;
        let nxdeis : Vertex = this.deiseil;
        let nxtuat : Vertex = this.tuathail;
        while (dodeis || dotuat){

        }
        return(this);
    }
    public exactive(index : number) : boolean {
        return(this.leastanc < index || this.children.lowest() < index);
    }
}

interface EnterInfo {
    comp : Component;
    index : number;
    depth : number;
    last : string;
}

interface ExitInfo {
    index : number;
    lowpoint : number;
    depth : number;
}

class Edge {
    public nodes : [Vertex, Vertex];
    private comp : Component;
    constructor(public graph : Graph, keys : [string, string]) {
        this.nodes = [
            graph.getNode(keys[0]).addEdge(this, keys[1], true),
            graph.getNode(keys[1]).addEdge(this, keys[0], false)
        ];
    }
    public reset() : Edge {
        this.comp = undefined;
        return(this);
    }
    public getNode(right : boolean){
        return(right ? this.nodes[1] : this.nodes[0]);
    }
    public visit(info : EnterInfo, right : boolean) : ExitInfo {
        this.comp = info.comp.addEdge(this);
        let nextNode = right ? this.nodes[1] : this.nodes[0];
        return(nextNode.visit(info));
    }
}

interface EdgeInfo {
    edge : Edge,
    right : boolean,
    back : boolean,
    lowpoint : number
}

class DFChild {
    public info : EdgeInfo;
    private next : DFChild;
    private last : DFChild;
    private unsetter : (edge : DFChild) => DFChildren;
    constructor(edgeInfo : EdgeInfo, public parent : DFChildren){
        this.info = edgeInfo;
        this.unsetter = () => this.parent;
    }
    public setLast(lastChild : DFChild) : DFChild {
        this.last = lastChild;
        return(this);
    }
    public setNext(nextChild : DFChild) : DFChild {
        this.next = nextChild;
        return(this);
    }
    public setFirst(handleUnset : (edge : DFChild) => DFChildren){
        this.unsetter = handleUnset;
        return(this);
    }
    public unchild(){
        this.last.setNext(this.next);
        this.next.setLast(this.last);
        this.unsetter(this.next);
        return(this);
    }
}
class DFChildren {
    private children : DFChild[] = [];
    private firstchild : DFChild;
    private sealed : boolean;
    constructor(){
        this.sealed = false;
    }
    public addChild(edge : EdgeInfo) : DFChildren {
        if (!this.sealed){
            this.children.push(new DFChild(edge, this));
        }
        return(this);
    }
    private setFirst(edge : DFChild) : DFChildren {
        this.firstchild = edge.setFirst(this.setFirst);
        return(this);
    }
    private sort() : DFChildren {
        let len : number = this.children.length;
        let idxs : number[] = this.children.map(
            (child, index) => index
        );
        let mods : number[] = this.children.map(
            child => child.info.lowpoint
        );
        let anymod : boolean = true;
        let jagidx : number[][];
        let jagmod : number[][];
        let mod : number;
        let rmd : number;
        let islot : number;
        let ilen : number;
        while(anymod){
            anymod = false;
            jagidx = [[],[],[],[],[],[],[],[],[],[]];
            jagmod = [[],[],[],[],[],[],[],[],[],[]];
            for (let i = 0; i < len; i++){
                mod = Math.floor(mods[i] / 10);
                rmd = mods[i] % 10;
                if (!anymod && mod > 0){ anymod = true; }
                jagidx[rmd].push(idxs[i]);
                jagmod[rmd].push(mod);
            }
            islot = 0;
            for (let i = 0; i < 10; i++){
                ilen = jagmod[i].length;
                idxs.splice(islot, ilen, ...jagidx[i]);
                mods.splice(islot, ilen, ...jagmod[i]);
                islot = islot + ilen;
            }
        }
        this.children = idxs.map(
            idx => this.children[idx]
        );
        this.setFirst(
            this.children[0].setLast(
                this.children[this.children.length -1]
            ).setNext(
                this.children[1]
            )
        );
        for (let i : number = 1; i < this.children.length - 1; i++){
            this.children[i].setLast(
                this.children[i - 1]
            ).setNext(
                this.children[i + 1]
            )
        }
        this.children[this.children.length - 1].setLast(
            this.children[this.children.length - 2]
        ).setNext(
            this.children[0]
        );
        return(this);
    }
    public seal() : DFChildren {
        if (!this.sealed) {
            this.sort();
            this.sealed = true;
        }
        return(this);
    }
    public lowest() : number {
        return(this.firstchild.info.lowpoint);
    }
}

class Component {
    public nodes : Vertex[];
    public edges : Edge[];
    constructor(public graph : Graph, public root : Vertex) {
        this.nodes = [];
        this.edges = [];
        let visInfo : EnterInfo = {
            comp : this,
            index : 0,
            depth : 0,
            last : undefined
        }
        root.visit(visInfo);
    }
    public addEdge(edge : Edge) : Component {
        this.edges.push(edge);
        return(this)
    }
    public addNode(node : Vertex) : Component {
        this.nodes.push(node);
        return(this)
    }
}

export default class Graph {
    public nodes : {[key : string] : Vertex};
    public unvis : {[key : string] : Vertex};
    public edges : Edge[];
    public comps : Component[];
    private sealed : boolean = false;
    constructor(){
        this.nodes = {};
        this.unvis = {};
        this.edges = [];
        this.comps = [];
    }
    public unseal() : Graph {
        if (this.sealed){
            this.sealed = false;
            this.comps = [];
            for (let key in this.nodes){
                this.unvis[key] = this.nodes[key].reset();
            }
            for (let idx in this.edges){
                this.edges[idx].reset();
            }
        }
        return(this);
    }
    public seal() : Graph {
        if (!this.sealed){
            this.sealed = true;
            this.comps = [];
            for (let key in this.unvis){
                this.comps.push(new Component(this, this.unvis[key]));
            }
        }
        return(this)
    }
    public addNode(key : string) : Vertex {
        if (!this.sealed){
            let newNode = new Vertex(this, key);
            this.nodes[key] = newNode;
            this.unvis[key] = newNode;
            return(newNode);
        }
    }
    public getNode(key : string) : Vertex {
        return(this.nodes[key]);
    }
    public addEdge(keys : [string, string]) : Edge {
        if (!this.sealed){
            let newEdge = new Edge(this, keys);
            this.edges.push(newEdge);
            return(newEdge);
        }
    }
    public didVisit(key : string) : void {
        delete this.unvis[key];
    }
}